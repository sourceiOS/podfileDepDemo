**podfileDep利用ruby和yaml，托管了iOS端依赖库管理，具备了以下优点**
1. 使用yaml文件配置依赖，PodfileDep使用ruby语言解析依赖，制定一定的编写规则，灵活又不失规范
2. 新增本地依赖管理逻辑，可用作开发依赖使用
3. 二方库和三方库分开，方便管理
4. yaml文件的方便解析特性，有助于CI/CD对于依赖的增删改查
5. pod install时日志输出更多的依赖库信息，例如间接被依赖的组件，未被使用的依赖库
6. pod install时帮助检查应该使用<>导入而使用""导入的头文件

### 如何使用？

#### 1、安装依赖
```
gem install podfileDep
```
#### 2、Podfile改造
[点击查看](https://gitee.com/sourceiOS/podfileDepDemo/blob/master/Podfile)

#### 3、重新pod install
```
pod install
```


### 远程依赖如何更新升级?
```
gem uninstall podfileDep && gem install podfileDep
```

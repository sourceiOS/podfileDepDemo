//
//  AppDelegate.h
//  Runner
//
//  Created by wangshuaipeng on 2023/2/2.
//  
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface AppDelegate : UIResponder<UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end
NS_ASSUME_NONNULL_END

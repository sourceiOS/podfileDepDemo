//
//  main.m
//  Runner
//
//  Created by wangshuaipeng on 2022/12/30.
//

#import <UIKit/UIKit.h>

int main(int argc, char * argv[]) {
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, @"AppDelegate");
    }
}

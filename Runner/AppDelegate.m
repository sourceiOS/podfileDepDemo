//
//  AppDelegate.m
//  Runner
//
//  Created by wangshuaipeng on 2023/2/2.
//  
//

#import "AppDelegate.h"

@interface AppDelegate()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    UIViewController *vc = [[UIViewController alloc] init];
    
    vc.title = @"成功";
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
            
    self.window.rootViewController = nav;
    
    self.window.backgroundColor = [UIColor whiteColor];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}


@end
